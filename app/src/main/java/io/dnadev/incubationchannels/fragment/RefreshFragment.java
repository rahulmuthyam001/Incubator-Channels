package io.dnadev.incubationchannels.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by rahulmuthyam on 13/03/18.
 * Helper Fragment
 */

public abstract class RefreshFragment extends Fragment {
    public abstract void refresh();
}
