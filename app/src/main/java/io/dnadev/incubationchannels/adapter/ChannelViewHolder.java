package io.dnadev.incubationchannels.adapter;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.dnadev.incubationchannels.R;

/**
 * Created by rahulmuthyam on 13/03/18.
 * ChannelListAdapter View Holder
 */

public class ChannelViewHolder extends RecyclerView.ViewHolder {

    View root;
    @BindView(R.id.iv_channel) AppCompatImageView iv_pic;
    @BindView(R.id.tv_channel_name) TextView tv_name;
    @BindView(R.id.tv_channel_type) TextView tv_type;

    public ChannelViewHolder(View itemView) {
        super(itemView);
        root = itemView;
        ButterKnife.bind(this, itemView);
    }
}
