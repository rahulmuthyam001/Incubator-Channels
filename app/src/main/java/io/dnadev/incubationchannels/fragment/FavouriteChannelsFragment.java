package io.dnadev.incubationchannels.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.dnadev.incubationchannels.R;
import io.dnadev.incubationchannels.activity.MainActivity;
import io.dnadev.incubationchannels.adapter.ChannelListAdapter;
import io.dnadev.incubationchannels.model.Channel;

/**
 * Created by rahulmuthyam on 13/03/18.
 * Fragment to display favourite channels
 */

public class FavouriteChannelsFragment extends RefreshFragment {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private MainActivity activity;

    public static FavouriteChannelsFragment newInstance() {
        Bundle args = new Bundle();
        FavouriteChannelsFragment fragment = new FavouriteChannelsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_channels_all, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        refresh();
        return view;
    }

    @Override
    public void refresh() {
        if (activity == null) return;
        ArrayList<Channel> favsList = new ArrayList<>();
        for (Channel channel : activity.getMemoryCache().getChannels()) if (channel.isFavourite()) favsList.add(channel);
        ChannelListAdapter adapter = new ChannelListAdapter(activity, favsList, "favourites");
        recyclerView.setAdapter(adapter);
    }
}
