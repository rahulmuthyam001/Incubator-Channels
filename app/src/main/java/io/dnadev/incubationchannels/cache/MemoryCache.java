package io.dnadev.incubationchannels.cache;

import android.util.Log;

import java.util.ArrayList;

import io.dnadev.incubationchannels.model.Channel;

/**
 * Created by rahulmuthyam on 14/03/18.
 */

public class MemoryCache {
    private ArrayList<Channel> cache = new ArrayList<>();

    public void addChannels(ArrayList<Channel> channels) {
        this.cache. addAll(channels);
    }

    public ArrayList<Channel> getChannels() {
        return cache;
    }

    public ArrayList<Channel> getChannels(int startIndex, int toIndex) {
        if(startIndex<0) startIndex = 0;
        if(toIndex>=cache.size()) toIndex = cache.size() - 1;
        Log.wtf("Cache", "Fetch objects from index " + startIndex + " to " + toIndex);
        return new ArrayList<>(cache.subList(startIndex, toIndex + 1));
    }
}
