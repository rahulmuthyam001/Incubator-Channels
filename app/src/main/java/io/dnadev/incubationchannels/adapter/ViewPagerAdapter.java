package io.dnadev.incubationchannels.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import io.dnadev.incubationchannels.fragment.RefreshFragment;

/**
 * Created by rahulmuthyam on 13/03/18.
 * PagerAdapter for MainActivity ViewPagerView
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<RefreshFragment> fragments = new ArrayList<>();
    private ArrayList<String> titles = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    public void addFragment(RefreshFragment fragment, String title) {
        fragments.add(fragment);
        titles.add(title);
        notifyDataSetChanged();
    }

    public void refreshFragments() {
        for (RefreshFragment refreshFragment : fragments) refreshFragment.refresh();
    }
}
