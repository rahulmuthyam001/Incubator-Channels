package io.dnadev.incubationchannels.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.dnadev.incubationchannels.R;
import io.dnadev.incubationchannels.adapter.ViewPagerAdapter;
import io.dnadev.incubationchannels.cache.MemoryCache;
import io.dnadev.incubationchannels.fragment.AllChannelsFragment;
import io.dnadev.incubationchannels.fragment.FavouriteChannelsFragment;
import io.dnadev.incubationchannels.model.Channel;
import io.dnadev.incubationchannels.rest.GetChannels;

/*
 * Main Activity
 */

public class MainActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    private ViewPagerAdapter viewPagerAdapter;
    private MemoryCache memoryCache = new MemoryCache();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initViews();
        fetchChannels();
    }

    private void initViews() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(AllChannelsFragment.newInstance(), "All Channels");
        viewPagerAdapter.addFragment(FavouriteChannelsFragment.newInstance(), "Favourites");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void fetchChannels() {
        showProgress(true, "Fetching channels...");
        new GetChannels(new GetChannels.Callback() {
            @Override
            public void onFinish(ArrayList<Channel> channels) {
                showProgress(false, null);
                memoryCache.addChannels(channels);
                refreshFragments();
            }

            @Override
            public void onError(int code, String message) {
                showProgress(false, null);
                new AlertDialog.Builder(viewPager.getContext())
                        .setTitle("Couldn't fetch channels!")
                        .setMessage("Code : " + code + "\nReason : " + message)
                        .setCancelable(false)
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fetchChannels();
                            }
                        }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();
            }
        }).execute();
    }

    public MemoryCache getMemoryCache() {
        return memoryCache;
    }

    public void refreshFragments() {
        viewPagerAdapter.refreshFragments();
    }

    public void updateFavouritesFragment() {
        ((FavouriteChannelsFragment) viewPagerAdapter.getItem(1)).refresh();
    }


    private void showProgress(boolean show, String text) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (!show) return;
        progressDialog = new ProgressDialog(viewPager.getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(text);
        progressDialog.show();
    }
}
