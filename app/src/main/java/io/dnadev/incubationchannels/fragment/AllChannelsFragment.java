package io.dnadev.incubationchannels.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.dnadev.incubationchannels.R;
import io.dnadev.incubationchannels.activity.MainActivity;
import io.dnadev.incubationchannels.adapter.ChannelListAdapter;
import io.dnadev.incubationchannels.cache.MemoryCache;

/**
 * Created by rahulmuthyam on 13/03/18.
 * Fragment used to display all available channels
 */

public class AllChannelsFragment extends RefreshFragment implements Runnable {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private MainActivity activity;
    private MemoryCache memoryCache;
    private boolean firstTime = true;
    private LinearLayoutManager layoutManager;
    private ChannelListAdapter adapter;

    //Scrolling
    private RecyclerView.OnScrollListener onScrollListener;
    private Handler handler = new Handler(Looper.getMainLooper());
    private volatile boolean fetchingTop = false;
    private volatile boolean fetchingBottom = false;

    public static AllChannelsFragment newInstance() {
        Bundle args = new Bundle();
        AllChannelsFragment fragment = new AllChannelsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        memoryCache = activity.getMemoryCache();

        View view = inflater.inflate(R.layout.fragment_channels_all, container, false);
        ButterKnife.bind(this, view);

        layoutManager = new LinearLayoutManager(recyclerView.getContext());
        adapter = new ChannelListAdapter(activity, "channels");
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(null);

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE: handler.postDelayed(AllChannelsFragment.this, 5000); break;
                    default: handler.removeCallbacks(AllChannelsFragment.this);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstPos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastVisibleItemPosition();

                //Scrolled Down
                if(dy>0 && lastPos==adapter.getRealItemCount()-1 && !fetchingBottom) {
                    //Check if last index exists
                    int lastPosIndex = adapter.getLastChannelIndex();
                    if(lastPosIndex==-1 || lastPosIndex==memoryCache.getChannels().size()-1) return;

                    //Fetch
                    fetchingBottom = true;
                    int startIndex = lastPosIndex + 1;
                    int toIndex = lastPosIndex + 5;
                    adapter.addChannelsLast(memoryCache.getChannels(startIndex, toIndex));
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyItemRangeChanged(0, adapter.getRealItemCount());
                            fetchingBottom = false;
                        }
                    });

                    //Scrolled Up
                } else if(dy<0 && firstPos==0 && !fetchingTop) {
                    //Check if first index exists
                    int firstPosIndex = adapter.getFirstChannelIndex();
                    if(firstPosIndex<=0) return;

                    //Fetch
                    fetchingTop = true;
                    int startIndex = firstPosIndex - 5;
                    int toIndex = firstPosIndex - 1;
                    adapter.addChannelsFirst(memoryCache.getChannels(startIndex, toIndex));
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyItemRangeChanged(0, adapter.getRealItemCount());
                            fetchingTop = false;
                        }
                    });
                }
            }
        };
        recyclerView.addOnScrollListener(onScrollListener);
        return view;
    }

    @Override
    public void refresh() {
        if (activity == null) return;
        if(firstTime) {
            adapter.setChannels(activity.getMemoryCache().getChannels(10, 25));
            Log.wtf("Init All-Channels", "Added objects from index 10 to 25");
            firstTime = false;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void run() {
        int fvip = layoutManager.findFirstVisibleItemPosition();
        int lvip = layoutManager.findLastVisibleItemPosition();
        final int firstPos = adapter.getChannelIndexAt(fvip);
        final int lastPos = adapter.getChannelIndexAt(lvip);

        if(!fetchingTop && !fetchingBottom && firstPos>=0 && lastPos>=0) {
            fetchingTop = true;
            fetchingBottom = true;

            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    adapter = new ChannelListAdapter(activity, memoryCache.getChannels(firstPos, lastPos), "channels");
                    recyclerView.setAdapter(adapter);
                    fetchingBottom = false;
                    fetchingTop = false;
                    Log.wtf("RecyclerView", "Cleared all objects except those from index " + firstPos + " to " + lastPos);
                }
            });
        }
    }
}
