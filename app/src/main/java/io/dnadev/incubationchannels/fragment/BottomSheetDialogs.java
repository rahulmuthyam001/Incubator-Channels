package io.dnadev.incubationchannels.fragment;

import android.content.res.ColorStateList;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import io.dnadev.incubationchannels.R;
import io.dnadev.incubationchannels.activity.MainActivity;
import io.dnadev.incubationchannels.model.Channel;

/**
 * Created by rahulmuthyam on 14/01/18.
 */

public class BottomSheetDialogs {
    private MainActivity activity;

    public BottomSheetDialogs(MainActivity activity) {
        this.activity = activity;
    }

    /*
     * Dialog to display Channel Info
     */
    public BottomSheetDialog viewChannelInfo(final Channel channel) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        final View view = LayoutInflater.from(activity).inflate(R.layout.bottomsheet_channel_info, null, false);

        final AppCompatImageView iv_pic = view.findViewById(R.id.iv_pic);
        final TextView tv_name = view.findViewById(R.id.tv_channel_name);
        final TextView tv_type = view.findViewById(R.id.tv_channel_type);
        final TextView tv_desc = view.findViewById(R.id.tv_channel_desc);
        final TextView tv_created = view.findViewById(R.id.tv_channel_created_by);
        final AppCompatImageButton bt_fav = view.findViewById(R.id.bt_fav);

        Glide.with(iv_pic).load(channel.getIconURL()).apply(RequestOptions.centerInsideTransform().error(R.drawable.default_channel)).into(iv_pic);
        tv_name.setText(channel.getName());
        tv_type.setText(channel.getType());
        tv_desc.setText(channel.getDescription());
        tv_created.setText("Created by " + channel.getCreator());
        bt_fav.setImageResource(channel.isFavourite() ? R.drawable.ic_heart : R.drawable.ic_heart_outline);
        bt_fav.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(view.getContext(), channel.isFavourite() ? R.color.colorAccent : R.color.f3)));

        bt_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                channel.setFavourite(!channel.isFavourite());
                bt_fav.setImageResource(channel.isFavourite() ? R.drawable.ic_heart : R.drawable.ic_heart_outline);
                bt_fav.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(v.getContext(), channel.isFavourite() ? R.color.colorAccent : R.color.f3)));
                Toast.makeText(v.getContext(), channel.getName() + (channel.isFavourite() ? " added " : " removed ") + "to favourites!", Toast.LENGTH_SHORT).show();
                activity.updateFavouritesFragment();
            }
        });

        bottomSheetDialog.setContentView(view);
        return bottomSheetDialog;
    }
}
